============
TED Download
============

This Python 2.x software allows you to easily download `TED <http://ted.com/>`_
talk videos along with subtitles and transcripts.

Usage
-----
Usage is pretty simple. Just paste the talk URL in the top input and
click `GO`. Then check the medias you want to download. Finally,
click `Download`.

Configuration
-------------
You can configure TED Download using ``config.yaml``:

::

    options:
        thumb_width: 160 # the width of the thumbnail of the talk
        thumb_ratio: 0.751 # thumb_height = thumb_width * thumb_ratio
        ask_directory: false # ask user to choose download directory; if false, uses download_dir
    
        download_dir: ./downloads
        transcript_use_odf: true # translate transcripts in ODF format; if false uses plain text

    styles: # style definitions for ODF output
        title:
            TextProperties:
                fontsize: 20pt
            ParagraphProperties:
                marginbottom: 1cm
        p:
            ParagraphProperties:
                lineheight: 0.5cm
                marginbottom: 10pt
                textalign: justify

License
-------
TED Download uses PyQt, thus it is under the GPL.

::

    Copyright (C) 2011  Alexandre `Zopieux` Macabies
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
