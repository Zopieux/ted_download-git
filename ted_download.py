#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# TED Download (for Python 2.x)
# Copyright (C) 2011  Alexandre `Zopieux` Macabies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt4 import QtCore, QtGui, QtNetwork
from BeautifulSoup import BeautifulSoup as BS
from ted2srt import toList, toSRT, toPlainText, toParagraphs
from odf.opendocument import OpenDocumentText
from odf.text import P
import odf.style
import os
import re
import subprocess
import tempfile
import yaml

BASE_URL = 'http://www.ted.com'
RE_TALKID = re.compile(ur'var talkID\s*=\s*(\d+);').search
#RE_OFFSETS = map(re.compile, (ur'introDuration\s*:\s*(\d+)', ur'adDuration\s*:\s*(\d+)', ur'postAdDuration\s*:\s*(\d+)'))
RE_OFFSETS = map(re.compile, (ur'introDuration\s*:\s*(\d+)',))
RE_FD = re.compile(ur'fd:"([^"]+)"').search
RE_PD = re.compile(ur'pd:"([^"]+)"').search
RE_THUMB = re.compile('(\d+)x(\d+)').search

CONFIGRAW = yaml.load(open('config.yaml', 'r'))
CONFIG = CONFIGRAW['options']
STYLES = CONFIGRAW['styles']

def styles_builder():
    styles = {}
    for name, builds in STYLES.iteritems():
        styler = odf.style.Style(name=name, family="paragraph")

        for builder, attributes in builds.iteritems():
            stylebuild = getattr(odf.style, builder)(attributes=attributes)
            styler.addElement(stylebuild)

        styles[name] =  styler
    return styles

class PixmapViewer(QtGui.QLabel):
    def __init__(self, parent=None):
        super(PixmapViewer, self).__init__(parent=parent)
        self.networkManager_ = QtNetwork.QNetworkAccessManager(self)
        self.networkManager_.finished.connect(self.pixmapReceived)

    def clear(self):
        self.setPixmap(QtGui.QPixmap())

    def setRemotePixmap(self, url):
        self.url = url
        try:
            w, h = map(float, RE_THUMB(url).groups())
            self._ratio = h / w
        except (AttributeError, ValueError, ZeroDivisionError):
            self._ratio = CONFIG.getfloat('Interface', 'thumb_ratio')

        self.networkManager_.get(QtNetwork.QNetworkRequest(QtCore.QUrl(url)))

    def pixmapReceived(self, reply):
        if reply.error() != QtNetwork.QNetworkReply.NoError:
            reply.deleteLater()
            return
        data = reply.readAll()
        if not data.size():
            return
        pixmap = QtGui.QPixmap()
        pixmap.loadFromData(data)
        thumbw = CONFIG.get('thumb_width')
        pixmap = pixmap.scaled(thumbw, thumbw * self._ratio, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
        self.setFixedSize(thumbw, thumbw * self._ratio)
        self.setPixmap(pixmap)
        reply.deleteLater()

class DownloadManager(QtGui.QWidget):
    finishedDownload = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(DownloadManager, self).__init__(parent=parent)

        ly = QtGui.QHBoxLayout()
        self.totalProgress = QtGui.QProgressBar()
        self.totalProgress.setFixedWidth(70)
        self.totalProgress.setFormat('%v/%m')
        self.totalProgress.setAlignment(QtCore.Qt.AlignCenter)
        self.fileProgress = QtGui.QProgressBar()
        self.fileProgress.setAlignment(QtCore.Qt.AlignCenter)
        lab = QtGui.QLabel(u"Progress:")
        ly.addWidget(self.totalProgress)
        ly.addWidget(lab)
        ly.addWidget(self.fileProgress)
        self.setLayout(ly)

        self.networkmanager = QtNetwork.QNetworkAccessManager(self)

    def startDownloads(self, url_list):
        self.urls = url_list
        self.totalProgress.setRange(0, len(self.urls))
        self.totalProgress.setValue(0)

        self.startNextDownload()

    def startNextDownload(self):
        try:
            self.currentMedia = self.urls.pop(0)
        except IndexError: # finished!
            return

        request = QtNetwork.QNetworkRequest()
        request.setUrl(QtCore.QUrl(self.currentMedia.url))
        request.setRawHeader('User-Agent', 'Mozilla/5.0 (compatible)')

        self.currentMedia.initiate()
        self.currentMedia.finishedPostProcess.connect(self.reallyFinished)

        self.reply = self.networkmanager.get(request)
        self.reply.error.connect(self.errorDisplay)
        self.reply.finished.connect(self.finished)
        self.reply.readyRead.connect(self.readyRead)
        self.reply.downloadProgress.connect(self.downloadProgress)

    def errorDisplay(self, replyerror):
        QtGui.QMessageBox.critical(self, "Error while downloading", self.reply.errorString())

    def readyRead(self):
        self.currentMedia.readyRead(self.reply)

    def downloadProgress(self, c, t):
        if t < 1:
            self.fileProgress.setRange(0, 0)
        else:
            self.fileProgress.setMaximum(t)
            self.fileProgress.setValue(c)

    def finished(self):
        redirectUrl = self.reply.attribute(QtNetwork.QNetworkRequest.RedirectionTargetAttribute).toUrl()
        if redirectUrl.toString():
            if isinstance(self.currentMedia, VideoAudioType):
                new = VideoAudioType(redirectUrl, self.currentMedia.filename)
                self.urls.insert(0, new)
                self.totalProgress.setValue(self.totalProgress.value() - 1)
        self.currentMedia.finished()

    def reallyFinished(self):
        self.fileProgress.setValue(0)
        if self.urls:
            self.totalProgress.setValue(self.totalProgress.value() + 1)
            self.startNextDownload()
        else:
            self.totalProgress.setValue(self.totalProgress.maximum())
            self.finishedDownload.emit()

class MediaType(QtCore.QObject):
    finishedPostProcess = QtCore.pyqtSignal()

    def __init__(self, url, filename, *args, **kwargs):
        super(QtCore.QObject, self).__init__()

        self.url = url
        self.filename = filename
        self.args = args
        self.kwargs = kwargs

    def initiate(self):
        raise NotImplementedError

    def readyRead(self, reply):
        raise NotImplementedError

    def finished(self):
        raise NotImplementedError

class VideoAudioType(MediaType):
    def initiate(self):
        self.filehandle = QtCore.QFile(self.filename)
        self.filehandle.open(QtCore.QIODevice.WriteOnly)

    def readyRead(self, reply):
        self.filehandle.write(reply.readAll())

    def finished(self):
        self.filehandle.close()
        self.filehandle.deleteLater()
        self.finishedPostProcess.emit()

class SubtitleType(MediaType):
    styles = styles_builder()

    def initiate(self):
        self.rawdata = QtCore.QBuffer()
        self.rawdata.open(QtCore.QIODevice.ReadWrite)

    def readyRead(self, reply):
        self.rawdata.write(reply.readAll())

    def finished(self):
        """
        
        """
        data = self.rawdata.data().data().decode('utf-8')
        offset = self.kwargs.get('offset', 0)
        stringList = tuple(toList(data, offset=offset))

        f = QtCore.QFile(unicode(self.filename) + '.srt')
        f.open(QtCore.QIODevice.WriteOnly)
        f.write(toSRT(stringList).encode('utf-8'))
        f.close()

        if self.kwargs.get('withTranscript', False):
            format = CONFIG.get('transcript_format', 'odf')
            assert format in ('odf', 'word', 'plain'), "transcript_format must be one of 'word', 'word' or 'plain'"
            if format in ('odf', 'word'):
                lines = toParagraphs(stringList)
                textdoc = OpenDocumentText()
                for item in self.styles.itervalues():
                    textdoc.styles.addElement(item)

                t = P(text=self.kwargs.get('title'), stylename=self.styles['title'])
                textdoc.text.addElement(t)
                for line in lines:
                    p = P(text=line, stylename=self.styles['p'])
                    textdoc.text.addElement(p)
                fname = unicode(self.filename)

                if format == 'word':
                    tmp = tempfile.TemporaryFile(suffix='.odt')
                    textdoc.save(tmp)
                    # TODO: make this non-blocking
                    p = subprocess.call([CONFIG.get('odf_to_word_path'), '/I', tmp.name, '/O', fname + '.doc'])
                    tmp.close()
                    if not 'Done.' in p:
                        QtGui.QMessageBox.warning(self, u"Error", u"Unable to convert transcript to Word format.\n"
                            u"Please check your odf_to_word_path config line.")
                else:
                    textdoc.save(fname + '.odt')
                    
            else: # plain
                wrapped = toPlainText(stringList)
                f = QtCore.QFile(unicode(self.filename) + '.txt')
                f.open(QtCore.QIODevice.WriteOnly)
                f.write(wrapped.encode('utf-8'))
                f.close()

        self.finishedPostProcess.emit()

class Mainwindow(QtGui.QDialog):
    def __init__(self):
        super(Mainwindow, self).__init__(None, QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowMinMaxButtonsHint)
        self.setWindowTitle("TED Downloader")
        icon = QtGui.QIcon('icon.ico')
        self.setWindowIcon(icon)

        mainLayout = QtGui.QVBoxLayout()

        self.gb0 = QtGui.QGroupBox("URL")
        ly2 = QtGui.QHBoxLayout()
        lab1 = QtGui.QLabel("Talk URL: ")
        ly2.addWidget(lab1)
        self.urlField = QtGui.QLineEdit()
        ly2.addWidget(self.urlField)
        self.goButton = QtGui.QPushButton("GO")
        self.goButton.setDefault(False)
        self.goButton.setAutoDefault(False)
        ly2.addWidget(self.goButton)
        self.gb0.setLayout(ly2)
        mainLayout.addWidget(self.gb0)

        self.gb1 = QtGui.QGroupBox("Media information")
        ly1 = QtGui.QHBoxLayout()
        self.thumbPreview = PixmapViewer()
        self.thumbPreview.setFixedSize(CONFIG.get('thumb_width'), CONFIG.get('thumb_width') * CONFIG.get('thumb_ratio'))
        ly1.addWidget(self.thumbPreview)
        self.infoLabel = QtGui.QLabel()
        self.infoLabel.setAlignment(QtCore.Qt.AlignTop)
        self.infoLabel.setWordWrap(True)
        ly1.addWidget(self.infoLabel)
        self.gb1.setLayout(ly1)
        mainLayout.addWidget(self.gb1)

        self.gb2 = QtGui.QGroupBox("Download selection")
        ly3 = QtGui.QHBoxLayout()
        ly31 = QtGui.QVBoxLayout()
        ly31.setAlignment(QtCore.Qt.AlignTop)
        self.groupAudio = QtGui.QButtonGroup()
        self.groupAudio.setExclusive(False)
        self.audioNormal = QtGui.QCheckBox("Normal audio")
        self.groupAudio.addButton(self.audioNormal)
        ly31.addWidget(self.audioNormal)

        ly31.addStretch()

        self.groupVideo = QtGui.QButtonGroup()
        self.groupVideo.setExclusive(False)
        self.videoLow = QtGui.QCheckBox("Low-def video")
        self.videoNormal = QtGui.QCheckBox("Normal video")
        self.videoHD = QtGui.QCheckBox("HD video")
        self.groupVideo.addButton(self.videoLow)
        self.groupVideo.addButton(self.videoNormal)
        self.groupVideo.addButton(self.videoHD)
        ly31.addWidget(self.videoLow)
        ly31.addWidget(self.videoNormal)
        ly31.addWidget(self.videoHD)

        ly32 = QtGui.QVBoxLayout()
        self.subtitleList = QtGui.QListWidget()
        self.subtitleList.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        ly32.addWidget(self.subtitleList)
        self.downloadTranscript = QtGui.QCheckBox(u"Download transcripts too")
        ly32.addWidget(self.downloadTranscript)

        self.groupAudio.buttonClicked.connect(self.checkDownloadButton)
        self.groupVideo.buttonClicked.connect(self.checkDownloadButton)
        self.subtitleList.itemSelectionChanged.connect(self.checkDownloadButton)

        ly3.addLayout(ly32)
        ly3.addLayout(ly31)
        self.gb2.setLayout(ly3)
        mainLayout.addWidget(self.gb2)

        self.gb3 = QtGui.QGroupBox("Download")
        ly4 = QtGui.QHBoxLayout()
        self.downloadManager = DownloadManager()
        self.downloadButton = QtGui.QPushButton("Download now!")
        self.downloadButton.setDefault(False)
        self.downloadButton.setAutoDefault(False)
        ly4.addWidget(self.downloadManager)
        ly4.addWidget(self.downloadButton)
        self.gb3.setLayout(ly4)
        mainLayout.addWidget(self.gb3)

        self.goButton.clicked.connect(self.loadPage)
        self.urlField.returnPressed.connect(self.loadPage)
        self.downloadButton.clicked.connect(self.downloadNow)
        self.downloadManager.finishedDownload.connect(self.downloadDone)

        self.pageGetter = QtNetwork.QNetworkAccessManager()
        self.pageGetter.finished.connect(self.pageLoaded)
        self.initData()

        self.setLayout(mainLayout)
        self.urlField.setFocus()

        self.setPanelState(False)
        self.gb0.setEnabled(True)

    def slugify(self, string):
        string = re.sub(ur'\s+', '_', string)
        string = re.sub(ur'[^\w.-]', '', string)
        return string.strip(u'_.- ').lower()

    def slugify_spaced(self, string):
        string = re.sub(ur'[^\w\s\.\-]', '', string)
        string = re.sub(ur'\s+', ' ', string)
        return string.strip(u'_.- ').title()

    def displayError(self, error):
        QtGui.QMessageBox.critical(self, self.windowTitle(), error)

    def setPanelState(self, state):
        self.gb0.setEnabled(state)
        self.gb1.setEnabled(state)
        self.gb2.setEnabled(state)
        self.gb3.setEnabled(state)

    def initData(self):
        self.pageUrl = None
        self.pageData = None
        self.pageSoup = None
        self.talkID = None
        self.talkName = None
        self.downloadLinks = {
            'audio': {'normal': None,},
            'video': {'normal': None, 'hd': None,},
        }
        self.availableSubtitles = {}

    def clearOptions(self):
        for button in self.groupAudio.buttons():
            button.setChecked(False)
        for button in self.groupVideo.buttons():
            button.setChecked(False)
        self.downloadTranscript.setChecked(False)
        self.thumbPreview.clear()
        self.infoLabel.clear()
        self.subtitleList.clear()
        self.initData()

    def checkDownloadButton(self, *args):
        # check if something is selected
        some_subtitles = bool(self.subtitleList.selectedItems())
        some_audio = self.groupAudio.checkedId() != -1
        some_video = self.groupVideo.checkedId() != -1

        self.downloadButton.setEnabled(any((some_subtitles, some_audio, some_video)))

    def browseToPage(self):
        pass
        #self.pageUrl

    def loadPage(self):
        url = QtCore.QUrl.fromUserInput(self.urlField.text())
        if not url.isValid() or not url.host() or not url.host().endsWith('ted.com'):
            return
        self.setPanelState(False)
        self.clearOptions()
        self.pageUrl = url
        self.pageGetter.get(QtNetwork.QNetworkRequest(QtCore.QUrl(self.pageUrl)))

    def pageLoaded(self, reply):
        if reply.error() != QtNetwork.QNetworkReply.NoError:
            reply.deleteLater()
            self.displayError(u"Unable to retrieve video details.")
            return

        self.pageData = reply.readAll().data().decode('utf-8')
        self.pageSoup = BS(self.pageData)

        # general info
        try:
            self.talkID = RE_TALKID(self.pageData).groups()[0]
            self.talkName = self.pageSoup.find('span', {'id': 'altHeadline'}).string

            i_fd = RE_FD(self.pageData).groups()[0]
            i_pd = RE_PD(self.pageData).groups()[0]
            about = self.pageSoup.find('p', {'id':'tagline'}).string

        except Exception:
            self.displayError("This is not a valid TED video page.")
            self.clearOptions()
            self.gb0.setEnabled(True)
            return

        self.infoLabel.setText(
            u'<h3>{0}</h3><p>#{4} – Filmed: {1} – Published: {2}</p><p>{3}</p>'.format(self.talkName, i_fd, i_pd, about, self.talkID)
        )

        self.offset = sum(map(lambda _: int(_.search(self.pageData).groups()[0]), RE_OFFSETS))/1000.

        # check download availability
        a = self.pageSoup.find('a', text=u'Audio to desktop (MP3)')
        if a: self.downloadLinks['audio']['normal'] = a.parent['href']
        self.audioNormal.setEnabled(bool(a))
        a = self.pageSoup.find('a', text=u'Download to desktop (MP4)')
        if a: self.downloadLinks['video']['normal'] = a.parent['href']
        self.videoNormal.setEnabled(bool(a))
        a = self.pageSoup.find('a', text=u'Low-res video (MP4)')
        if a: self.downloadLinks['video']['low'] = a.parent['href']
        self.videoLow.setEnabled(bool(a))
        a = self.pageSoup.find('a', text=u'High-res video (MP4)')
        if a: self.downloadLinks['video']['hd'] = a.parent['href']
        self.videoHD.setEnabled(bool(a))

        # populate subtitles
        lngselect = self.pageSoup.find('select', {'id': 'languageCode'})
        for opt in lngselect.findAll('option', recursive=False):
            code = opt['value']
            name = opt.string
            self.availableSubtitles[code] = name

            item = QtGui.QListWidgetItem(name, parent=self.subtitleList)
            item.setData(QtCore.Qt.UserRole, code)
            if opt.get('selected', False):
                font = item.font()
                font.setBold(True)
                item.setFont(font)
            self.subtitleList.addItem(item)

        self.downloadTranscript.setEnabled(self.subtitleList.count() > 0)
        self.checkDownloadButton()
        self.thumbPreview.setRemotePixmap(self.pageSoup.find('link', {'rel': 'image_src'})['href'])
        self.setPanelState(True)

    def downloadNow(self):
        if CONFIG.get('ask_directory'):
            directory = QtGui.QFileDialog.getExistingDirectory(self, u"Choose destination directory",
                            QtCore.QDir.currentPath(), QtGui.QFileDialog.ShowDirsOnly)
            if not directory:
                return
            directory = QtCore.QDir(directory)
        else:
            # download_dir may still be absolute!
            dir = os.path.normpath(os.path.join(os.path.dirname(__file__), CONFIG.get('download_dir')))
            directory = QtCore.QDir(dir)
            newdir = self.slugify_spaced(self.talkName)
            a = directory.mkdir(newdir)
            a = directory.cd(newdir)

        url_list = []
        transcript = self.downloadTranscript.isChecked()

        # subtitles
        for item in self.subtitleList.selectedItems():
            code = item.data(QtCore.Qt.UserRole).toString()
            url_list.append(SubtitleType(
                QtCore.QUrl('http://www.ted.com/talks/subtitles/id/%s/lang/%s' % (self.talkID, code)),
                directory.filePath('%s.%s' % (self.slugify(self.talkName), code)),
                offset=self.offset,
                title=self.talkName,
                withTranscript=transcript
            ))

        # medias
        if self.audioNormal.isChecked():
            url_list.append(VideoAudioType(
                QtCore.QUrl(self.downloadLinks['audio']['normal']),
                directory.filePath('%s.mp3' % self.slugify(self.talkName))
            ))
        if self.videoNormal.isChecked():
            url_list.append(VideoAudioType(
                QtCore.QUrl(self.downloadLinks['video']['normal']),
                directory.filePath('%s.mp4' % self.slugify(self.talkName))
            ))
        if self.videoHD.isChecked():
            url_list.append(VideoAudioType(
                QtCore.QUrl(self.downloadLinks['video']['hd']),
                directory.filePath('%s.hd.mp4' % self.slugify(self.talkName))
            ))
        if self.videoLow.isChecked():
            url_list.append(VideoAudioType(
                QtCore.QUrl(self.downloadLinks['video']['low']),
                directory.filePath('%s.low.mp4' % self.slugify(self.talkName))
            ))

        if not url_list:
            return

        self.setPanelState(False)
        self.gb3.setEnabled(True)
        self.downloadButton.setEnabled(False)
        self.gb3.setTitle(u"Download – Running…")

        self.downloadManager.startDownloads(url_list)

    def downloadDone(self):
        self.gb3.setTitle(u"Download")
        QtGui.QMessageBox.information(self, self.windowTitle(), u"All downloads finished successfully.")
        self.setPanelState(True)
        self.checkDownloadButton()

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    win = Mainwindow()
    win.show()
    sys.exit(app.exec_())
