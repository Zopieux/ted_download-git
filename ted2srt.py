#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# TED Download (for Python 2.x)
# Copyright (C) 2011  Alexandre `Zopieux` Macabies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import textwrap
from datetime import datetime, timedelta

class SrtItem(object):
    def __init__(self, datadict, offset=0):
        start = datadict.get('startTime', None)
        duration = datadict.get('duration', None)
        text = datadict.get('content', None)
        newPara = bool(datadict.get('startOfParagraph', False))

        if any((start is None, duration is None, text is None)):
            raise ValueError("Malformed subtitle: %s" % datadict)

        offset = SrtTime(offset)
        start = SrtTime(start/1000.)
        duration = SrtTime(duration/1000.)
        self.start = start + offset
        self.duration = duration + offset
        self.text = text
        self.newPara = newPara
        self.end = start + duration + offset

    def __unicode__(self):
        return u'<SrtItem %s + %s `%s`>' % (self.start, self.duration, self.text)

    def __repr__(self):
        return self.__unicode__()

class SrtTime(object):
    fool = datetime(1,1,1)

    def __init__(self, sec=0.):
        self.seconds = float(sec)

    def __add__(self, other):
        return SrtTime(self.seconds + other.seconds)

    @property
    def _dtobj(self):
        return self.fool + timedelta(seconds=self.seconds)

    @property
    def srtformat(self):
        return self._dtobj.time().strftime('%H:%M:%S,') + str(self._dtobj.time().microsecond/1000).zfill(3)

    def __repr__(self):
        return '<SrtTime %s>' % self.srtformat

def toList(jsonraw, offset=0):
    jsoned = json.loads(jsonraw)

    for line in jsoned['captions']:
        yield SrtItem(line, offset=offset)

def toSRT(pythonlist):
    indice = 1
    buff = u''

    for line in pythonlist:
        buff += u'{0}\n{1} --> {2}\n{3}\n\n'.format(indice, line.start.srtformat, line.end.srtformat, line.text)
        indice += 1

    return buff

def toParagraphs(pythonlist):
    buffer = u''
    for line in pythonlist:
        if line.newPara:
            yield buffer.strip()
            buffer = u''
        buffer += line.text + u' '
    yield buffer.strip()

def toPlainText(pythonlist, width=80):
    tw = textwrap.TextWrapper(width=width)
    final = u'\n\n'.join(tw.fill(line) for line in toParagraphs(pythonlist))
    return final

def convertRawToSRT(jsonraw, offset=0):
    return toSRT(toList(jsonraw, offset))

def test(fname):
    pylist = toList(open(fname, 'r').read())

    lol = open('test.out.srt', 'w')
    lol.write(toSRT(pylist).encode('utf-8'))
    lol.close()

if __name__ == '__main__':
    import sys
    test(sys.argv[1])
